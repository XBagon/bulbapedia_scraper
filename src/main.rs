use combine::char::string;
use combine::{any, attempt, between, choice, easy, error, many, none_of, not_followed_by, parser, sep_by, stream::Positioned, token, Parser};
use serde::Deserialize;
use serde_json as json;
use std::fs;
use std::path::Path;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let links: LinksResponse = reqwest::get(
        "https://bulbapedia.bulbagarden.net/w/api.php?action=parse&page=List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number&prop=links&format=json",
    )?
    .json()?;

    let pokemon_titles = links.parse.links.into_iter().map(|link| link.title).filter(|title| title.ends_with("(Pokémon)"));

    let folder_path = Path::new("bulbapedia_scraper_output");
    match fs::create_dir(folder_path) {
        Ok(()) => {}
        Err(ref e) if e.kind() == std::io::ErrorKind::AlreadyExists => {}
        res => res.unwrap(),
    }

    for title in pokemon_titles {
        let wikitext: WikitextResponse =
            reqwest::get(&format!("https://bulbapedia.bulbagarden.net/w/api.php?action=parse&page={}&prop=wikitext&format=json", title))?.json()?;
        let json = parse(&wikitext.parse.wikitext.text);

        fs::write(folder_path.join(format!("{}.json", title)), json.to_string()).unwrap();
    }
    Ok(())
}

#[derive(Deserialize, Debug)]
struct LinksResponse {
    parse: LinksParse,
}

#[derive(Deserialize, Debug)]
struct LinksParse {
    title: String,
    pageid: u32,
    links: Vec<Link>,
}

#[derive(Deserialize, Debug)]
struct Link {
    //ns: u32,
    //exists: Option<String>,
    #[serde(rename = "*")]
    title: String,
}

#[derive(Deserialize, Debug)]
struct WikitextResponse {
    parse: WikitextParse,
}

#[derive(Deserialize, Debug)]
struct WikitextParse {
    title: String,
    pageid: u32,
    wikitext: Wikitext,
}

#[derive(Deserialize, Debug)]
struct Wikitext {
    #[serde(rename = "*")]
    text: String,
}

#[derive(Debug)]
enum Parsed {
    Template(Vec<String>),
    Header(String),
    Other(char),
}

fn parse(input: &str) -> json::Value {
    let mut object_map = json::Map::new();
    let mut parser = many::<Vec<_>, _>(choice!(
        attempt(between(
            string("{{"),
            string("}}"),
            sep_by::<Vec<_>, _, _>(
                many::<String, _>(not_followed_by(string("}}").or(string("|"))).and(any()).map(|(_, c)| c)).map(|s| s.trim().to_owned()),
                string("|"),
            )
            .map(Parsed::Template),
        )),
        attempt(parse_header()),
        any().map(Parsed::Other)
    ));
    let result = parser.easy_parse(input);

    let parsed = match result {
        Ok(parsed) => parsed,
        Err(err) => {
            panic!("{}\nIndex: {}", err, err.position.translate_position(input));
        }
    };

    let mut cur_obj_map = &mut object_map;
    let mut rest_text = String::new();
    for item in parsed.0 {
        if let Parsed::Other(char) = item {
            rest_text.push(char);
        } else {
            let trimmed = rest_text.trim();
            if !trimmed.is_empty() {
                cur_obj_map
                    .entry(String::from("rest_text"))
                    .or_insert(json::Value::Array(Vec::new()))
                    .as_array_mut()
                    .unwrap()
                    .push(json::Value::String(trimmed.to_owned()));
            }
            rest_text = String::new();
            match item {
                Parsed::Header(header) => {
                    cur_obj_map = object_map.entry(header).or_insert(json::Value::Object(json::Map::new())).as_object_mut().unwrap();
                }
                Parsed::Template(template) => {
                    let mut iter = template.into_iter();
                    if let Some(ident) = iter.next() {
                        let mut map = json::Map::new();
                        for kv_pair in iter {
                            let mut kv_iter = kv_pair.split('=');
                            let key = kv_iter.next().unwrap();
                            if let Some(value) = kv_iter.next() {
                                let value = if let Ok(num) = <i64>::from_str(value) {
                                    json::Value::Number(num.into())
                                } else if let Ok(num) = <u64>::from_str(value) {
                                    json::Value::Number(num.into())
                                } else if let Ok(num) = <f64>::from_str(value) {
                                    json::Value::Number(json::Number::from_f64(num).unwrap())
                                } else {
                                    json::Value::String(value.to_owned())
                                };
                                map.insert(key.to_owned(), value);
                            } else {
                                let trimmed = key.trim();
                                if trimmed.is_empty() {
                                    continue;
                                }
                                map.entry("rest_array")
                                    .or_insert(json::Value::Array(Vec::new()))
                                    .as_array_mut()
                                    .unwrap()
                                    .push(json::Value::String(trimmed.to_owned()));
                            }
                        }

                        let value = json::Value::Object(map);
                        match cur_obj_map.entry(ident) {
                            json::map::Entry::Vacant(v) => {
                                v.insert(value);
                            }
                            json::map::Entry::Occupied(mut o) => match o.get_mut() {
                                json::Value::Array(arr) => {
                                    arr.push(value);
                                }
                                _ => {
                                    let object = o.get().clone(); //is this inefficient?
                                    o.insert(json::Value::Array(vec![object, value]));
                                }
                            },
                        }
                        //cur_obj_map.insert(ident, json::Value::Object(map));
                    }
                }
                _ => unreachable!(),
            }
        }
    }
    json::Value::Object(object_map)
}

fn parse_header<'a>() -> impl Parser<Input = easy::Stream<&'a str>, Output = Parsed> {
    parser(move |input: &mut easy::Stream<&str>| {
        let position = input.position();
        let delim_iter = token('=').iter(input);
        let count = delim_iter.count();
        if count > 2 {
            let mut iter = none_of(['='].iter().cloned()).iter(input);
            let result = iter.by_ref().collect();
            let (header, consumed) = iter.into_result(result)?;
            let mut delim_iter = token('=').iter(input);
            if delim_iter.by_ref().take(count).count() == count {
                Ok((header, consumed))
            } else {
                let errors = easy::Errors::new(position, error::StreamError::expected(From::from("=")));
                Err(error::Consumed::Empty(errors.into()))
            }
        } else {
            let errors = easy::Errors::new(position, error::StreamError::expected(From::from("==")));
            Err(error::Consumed::Empty(errors.into()))
        }
    })
    .map(Parsed::Header)
}
